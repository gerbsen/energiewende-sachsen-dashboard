venv: tools/python/requirements.txt
	[ -d ./venv ] || python3 -m venv venv
	./venv/bin/pip install --upgrade pip
	./venv/bin/pip install -Ur tools/python/requirements.txt
	touch venv

.PHONY: python-image
python-image:
	docker build -t klimadashboard/python:latest tools/python/

.PHONY: nginx-image
nginx-image:
	docker build -t klimadashboard/nginx:latest .