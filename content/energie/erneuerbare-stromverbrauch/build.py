import pandas as pd

url = "https://www.lak-energiebilanzen.de/ergebnisse-des-datenabrufs/?a=i200&j=1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018&l=14&v=ee_pro_strver_brutto"

df = pd.read_html(url, decimal=",", thousands=".")[0]
df.columns = ["Land", "Jahr", "Anteil"]

df = df[["Jahr", "Anteil"]]
df.to_csv("./erneuerbare-stromverbrauch.csv", index=False)
