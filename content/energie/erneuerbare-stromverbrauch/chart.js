document.addEventListener("DOMContentLoaded", function () {
  var vlSpec = {
    width: 'container',
    height: 'container',
    data: {
      url: '/energie/erneuerbare-stromverbrauch/erneuerbare-stromverbrauch.csv'
    },
    mark: { type: 'bar', tooltip: true },
    encoding: {
      y: { field: 'Anteil', type: 'quantitative' },
      x: { field: 'Jahr', type: 'ordinal' },
    }
  };

  vegaEmbed('#erneuerbare-stromverbrauch-chart', vlSpec);
})