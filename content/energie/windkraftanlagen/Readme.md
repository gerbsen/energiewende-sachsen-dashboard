# Windkraftanlagen (WKA) vom Marktstammdatenregister
Die Daten können vom Marktstammdatenregister unter [folgender URL](https://www.marktstammdatenregister.de/MaStR/Einheit/Einheiten/ErweiterteOeffentlicheEinheitenuebersicht) heruntergelanden werden.
Dabei ist darauf zu achten, dass man als Bundesland „Sachsen” und als Energieträger „Wind” einstellt und unbedingt in die **Erweiterte Einheitenübersicht** auswählt.
In der regulären Ansicht werden die Geokoordinaten nicht mit angezeigt. 
Mittels "Tabelle exportieren" kann man dann die Tabelle als CSV herunterladen. Zur Anzeige als Karte haben wir die Daten zu GeoJSON konvertiert.