# %%
import pandas as pd
import csv
import geopandas

# %%
df = pd.read_csv("mastr-windkraftanlagen.csv", sep=";", dtype="string")

# %%

datecols = [
    "Inbetriebnahmedatum der Einheit",
    "Meldedatum der Einheit",
    "Letzte Aktualisierung",
    "Datum der endgültigen Stilllegung",
    "Datum der geplanten Inbetriebnahme",
    "Inbetriebnahmedatum der EEG-Anlage",
    "Inbetriebnahmedatum der KWK-Anlage",
]
df[datecols] = df[datecols].apply(pd.to_datetime)

# %%

floats = ["Koordinate: Breitengrad (WGS84)", "Koordinate: Längengrad (WGS84)"]
df[floats] = df[floats].apply(
    lambda x: pd.to_numeric(x, errors="coerce").round(decimals=3)
)

# %%
fuckedup_floats = ["Bruttoleistung der Einheit", "Nettonennleistung der Einheit"]

for column in fuckedup_floats:
    tsd = df[column].str.match(r"\d+\.\d{3}$")
    df.loc[tsd, column] = df[column].str.replace(".", "")

df[fuckedup_floats] = df[fuckedup_floats].apply(pd.to_numeric)

# %%
columns_to_rename = {
    "Bruttoleistung der Einheit": "Bruttoleistung der Einheit (kW)",
    "Nettonennleistung der Einheit": "Nettonennleistung der Einheit (kW)",
}

df.rename(columns=columns_to_rename, inplace=True)


# %%
df.to_csv("windkraftanlagen.csv", sep=";", quoting=csv.QUOTE_ALL, index=False)

# %%

gdf = geopandas.GeoDataFrame(
    df,
    geometry=geopandas.points_from_xy(
        df["Koordinate: Längengrad (WGS84)"], df["Koordinate: Breitengrad (WGS84)"]
    ),
)

gdf[datecols] = gdf[datecols].apply(lambda x: x.dt.strftime("%Y-%m-%d"))

gdf.to_file("windkraftanlagen.geojson", driver="GeoJSON")
