document.addEventListener("DOMContentLoaded", function () {

  feather.replace()

  tippy('#bruttostromverbrauch-help', {
    content: "Der Bruttostromverbrauch umfasst den Endenergieverbrauch an Strom, sowie die damit einhergehenden Umwandlungs- und Übertragungsverluste. Er ist als Bezugsgröße für den Anteil der Erneuerbaren Energien in der EG-Richtlinie 2009/28/EG zur Förderung der Nutzung von Energie aus erneuerbaren Quellen festgelegt und neben dem Verbrauch von Wärme und Kraftstoffen ein wichtiger Bestandteil des Verbrauches von Endenergie. <br>Quelle: <a href='https://www.umweltbundesamt.de/service/glossar/b?tag=Bruttostromverbrauch#alphabar'>Umweltbundesamt</a>",
    allowHTML: true,
    interactive: true
  });

  mapboxgl.accessToken = 'pk.eyJ1IjoiZ2VyYiIsImEiOiJja2I1NnFrZGUwbXAzMnRuc3hqcnQ2NzQ0In0.35F0f6YiwAcyTd08MwLA0A';
  var map = new mapboxgl.Map({
    container: 'map-windkraftanlagen-sachsen', // container id
    style: 'mapbox://styles/mapbox/light-v10', // stylesheet location
    center: [13.458742, 50.9227525],
    zoom: 7
  });

  map.on('style.load', function () {
    map.addSource('mastr-windkraftanlagen', {
      'type': 'geojson',
      'data': '/energie/windkraftanlagen/windkraftanlagen.geojson'
    });

    map.addLayer({
      'id': 'mastr-windkraftanlagen-layer',
      'type': 'circle',
      'source': 'mastr-windkraftanlagen',
      'paint': {
        // make circles larger as the user zooms from z12 to z22
        'circle-radius': {
          'base': 5,
          'stops': [
            [5, 2],
            [22, 180]
          ]
        },
        'circle-color': '#F75590'
      }
    });
  });


  // display installation details in table 
  map.on('click', 'mastr-windkraftanlagen-layer', function (e) {
    var properties = e.features[0].properties
    var table =
      "<table class=\"table\">" +
      "<thead>" +
      "<tr>" +
      "<th scope=\"col\">Schlüssel</th>" +
      "<th scope=\"col\">Wert</th>" +
      "</tr>" +
      "</thead><tbody>";
    Object.keys(properties).forEach(key => {
      table +=
        "<tr>" +
        "<td>" + key + "</td>" +
        "<td>" + getPropertyValue(properties, key) + "</td>" +
        "</tr>"
    });

    description = table + "</tbody></table>";
    document.getElementById("windkraftanlagen-sachsen-wka-info").innerHTML = description;
  });


  function getPropertyValue(properties, key) {
    var value = properties[key]
    // workaround for https://github.com/mapbox/mapbox-gl-js/issues/8497
    value = (value == "null") ? "" : value
    if (value && ["Inbetriebnahmedatum der Einheit",
      "Meldedatum der Einheit",
      "Letzte Aktualisierung",
      "Inbetriebnahmedatum der EEG-Anlage",
      "Inbetriebnahmedatum der KWK-Anlage",
      "Datum der geplanten Inbetriebnahme",
      "Datum der endgültigen Stilllegung"].includes(key)) {
      value = moment(value).format("DD.MM.YYYY");
    }
    return value
  }

  // Change the cursor to a pointer when the mouse is over the places layer.
  map.on('mouseenter', 'mastr-windkraftanlagen-layer', function () {
    map.getCanvas().style.cursor = 'pointer';
  });

  map.on('mouseleave', 'mastr-windkraftanlagen-layer', function () {
    map.getCanvas().style.cursor = '';
  });

  // allow for switching between map and satellite layer
  var layerList = document.getElementById('map-windkraftanlagen-sachsen-menu');
  var inputs = layerList.getElementsByTagName('input');

  function switchLayer(layer) {
    var layerId = layer.target.id;
    map.setStyle('mapbox://styles/mapbox/' + layerId);
  }

  for (var i = 0; i < inputs.length; i++) {
    inputs[i].onclick = switchLayer;
  }
});