# %%
import pandas as pd
import fiona
import geopandas

# %%
dtypes = {
    "Betreiber": "str",
    "Adresse": "str",
    "Postleitzahl Ort": "str",
    "Bundesland": "str",
    "Längengrad [DG]": "float",
    "Breitengrad [DG]": "float",
    "Inbetriebnahmedatum": "str",
    "Anschlussleistung [kW]": "float",
    "Art der Ladeeinrichtung": "str",
    "Anzahl Ladepunkte": "Int64",
    "Steckertypen1": "str",
    "P1 [kW]": "float",
    "Public Key1": "str",
    "Steckertypen2": "str",
    "P2 [kW]": "float",
    "Public Key2": "str",
    "Steckertypen3": "str",
    "P3 [kW]": "float",
    "Public Key3": "str",
    "Steckertypen4": "str",
    "P4 [kW]": "float",
    "Public Key4": "str",
}

df = pd.read_csv(
    "./Ladesaeulenkarte_Datenbankauszug31-CSV.csv",
    float_precision="high",
    encoding="latin1",
    sep=";",
    decimal=",",
    na_values="-",
    header=None,
    skiprows=range(0, 6),
    skipfooter=0,
    names=dtypes.keys(),
    dtype=dtypes,
)

# %%
# some dude combined these values into one column :(
df[["Postleitzahl", "Ort"]] = df["Postleitzahl Ort"].str.extract(r"(\d{5})\s+(.+)")

# filter for saxony since the whole list is too large to download to the client
sn = df[df["Bundesland"] == "Sachsen"]

# %%
gemeinden = geopandas.read_file("/vsizip/vwg20200201_33_sachsen.zip/gem.shp")
gemeinden = gemeinden.to_crs("EPSG:4326")

gemeinden.rename(
    columns={
        "SCHLUESSEL": "ags",
        "ORTSNAME": "vwg_ort",
        "KREIS": "landkreis",
        "ADMIN": "admin",
        "SITZ_LRA": "sitz_lra",
    },
    inplace=True,
)


# %%
gsn = geopandas.GeoDataFrame(
    sn,
    geometry=geopandas.points_from_xy(sn["Längengrad [DG]"], sn["Breitengrad [DG]"]),
    crs="EPSG:4326",
)

result = geopandas.sjoin(
    gsn,
    gemeinden[["ags", "vwg_ort", "landkreis", "geometry"]],
    how="left",
    op="intersects",
)

# %%
# two locations are not within Sachsen, drop them
result = result.dropna(subset=["vwg_ort"])

# %%
# remove unneeded columns, and rename the rest
out_cols = {
    "Betreiber": "Betreiber",
    "Längengrad [DG]": "Längengrad",
    "Breitengrad [DG]": "Breitengrad",
    "ags": "AGS",
    "vwg_ort": "Ort",
    "landkreis": "Landkreis",
    "Inbetriebnahmedatum": "Inbetriebnahmedatum",
    "Anschlussleistung [kW]": "Anschlussleistung (kW)",
    "Art der Ladeeinrichtung": "Art der Ladeeinrichtung",
    "Anzahl Ladepunkte": "Anzahl Ladepunkte",
}

result = result[out_cols.keys()]
result.rename(columns=out_cols, inplace=True)

# %%
result.to_csv("./ladesaeulen.csv", index=False)
