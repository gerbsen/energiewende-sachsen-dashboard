document.addEventListener("DOMContentLoaded", function () {

  vegaEmbed("#ladesaeulen-chart-1", {
    title: {
      text: 'Anzahl der Ladesäulen',
      subtitle: 'nach Kreisen und kreisfreien Städten'
    },
    width: 'container',
    height: '200',
    data: {
      url: '/verkehr/ladesaeulen/ladesaeulen.csv'
    },
    mark: { type: 'bar', tooltip: true },
    encoding: {
      x: {
        field: 'Landkreis',
        type: 'nominal',
        sort: '-y',
        axis: { title: null }
      },
      y: {
        field: 'Landkreis',
        aggregate: 'count',
        type: 'quantitative',
        axis: { title: "Anzahl Ladesäulen" }
      }
    }
  })

  vegaEmbed("#ladesaeulen-chart-2", {
    title: {
      text: 'Summe der installierten Ladeleistung',
      subtitle: 'nach Kreisen und kreisfreien Städten'
    },
    width: 'container',
    height: '200',
    data: {
      url: '/verkehr/ladesaeulen/ladesaeulen.csv'
    },
    mark: { type: 'bar', tooltip: true },
    encoding: {
      x: {
        field: 'Landkreis',
        type: 'nominal',
        sort: '-y',
        axis: { title: null }
      },
      y: {
        field: "Anschlussleistung (kW)",
        aggregate: 'sum',
        type: 'quantitative',
        axis: { title: "Anschlussleistung (kW)" }
      }
    }
  })

  vegaEmbed("#ladesaeulen-chart-3", {
    title: {
      text: 'Anzahl der Ladesäulen nach Betreibern',
      subtitle: 'Alle Betreiber mit mindestens fünf Ladesäulen'
    },
    width: 'container',
    height: '200',
    data: {
      url: '/verkehr/ladesaeulen/ladesaeulen.csv'
    },
    mark: { type: 'bar', tooltip: true },
    transform: [
      {
        "aggregate": [
          { "op": "count", "field": "Betreiber", "as": "BetreiberCount" },
        ],
        "groupby": ["Betreiber"]
      },
      { "filter": "datum.BetreiberCount >= 5" }
    ],
    encoding: {
      x: {
        field: 'Betreiber',
        type: 'nominal',
        sort: '-y',
        axis: { title: null }
      },
      y: {
        field: 'BetreiberCount',
        type: 'quantitative',
        axis: { title: "Anzahl Ladesäulen" }
      }
    }
  })
})