# %%
import pandas as pd

dtypes = {
    "AGS": "string",
    "Name": "string",
    "Stichtag": "datetime64[ns]",
    "Gesamt": "Int32",
    "Benzin": "Int32",
    "Diesel": "Int32",
    "Gas": "Int32",
    "Hybrid": "Int32",  # ab 2017
    "Elektro": "Int32",  # ab 2017
    "dar. Erdgas": "Int32",  # bis 2016
    "Sonstige": "Int32",
}


df = pd.read_csv(
    "./46251-102Z.csv",
    engine="python",
    encoding="latin1",
    sep=";",
    na_values="-",
    header=None,
    skiprows=range(0, 8),
    skipfooter=4,
    names=dtypes.keys(),
    dtype=dtypes,
)

# cleanup region names
df["Name"] = df["Name"].str.strip()

# filter by years that have data for non-fossils, and ignore Erdgas column (too little data)
datefilter = df["Stichtag"] >= "2017-01-01"
columns = [column for column in df.columns if column != "dar. Erdgas"]
df = df[datefilter][columns]

# %%
# filter for state-level, remove unused columns, and arrange data in long form
sn = df[df["AGS"] == "14"]
sn = sn.drop("Gesamt", 1).iloc[:, 2:]
sn = sn.melt(id_vars="Stichtag")
sn.columns = ["Jahr", "Kraftstoffart", "Anzahl"]

# indicate if fossil or non-fossil
sn["Kategorie"] = sn["Kraftstoffart"].apply(
    lambda x: "Non-fossil" if x in ["Elektro", "Hybrid"] else "Fossil"
)
sn = sn[["Jahr", "Kraftstoffart", "Kategorie", "Anzahl"]]

# %%
# write results as CSV
sn.to_csv("./pkw-kraftstoffe.csv", index=False)
