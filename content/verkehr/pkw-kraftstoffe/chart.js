document.addEventListener("DOMContentLoaded", function () {

  var baseSpec = {
    width: 'container',
    height: 'container',
    data: {
      url: '/verkehr/pkw-kraftstoffe/pkw-kraftstoffe.csv'
    },
    mark: { type: 'bar', tooltip: true },
    encoding: {
      x: {
        field: 'Jahr',
        type: 'ordinal'
      },
      y: {
        field: 'Anzahl',
        type: 'quantitative',
      },
      color: {
        field: 'Kraftstoffart',
        type: 'nominal',
        scale: {
          scheme: 'tableau10' // TODO custom color scale?
        }
      }
    }
  }

  var fossilSpec = Object.assign(
    { transform: [{ filter: { field: 'Kategorie', equal: 'Fossil' } }] },
    baseSpec
  )

  var nonFossilSpec = Object.assign(
    { transform: [{ filter: { field: 'Kategorie', equal: 'Non-fossil' } }] },
    baseSpec
  )

  vegaEmbed("#pkw-kraftstoffe-chart-1", fossilSpec)
  vegaEmbed("#pkw-kraftstoffe-chart-2", nonFossilSpec)


})