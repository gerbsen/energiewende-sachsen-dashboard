# %%
import pandas as pd

# %%
df = pd.read_csv(
    "./46100-0015_flat.csv",
    encoding="latin1",
    sep=";",
    na_values=["x", "-", ".", "..."],
    dtype={
        "UNT003__Unternehmen__Anzahl": "Int32",
        "VER013__Befoerderte_Personen__1000": "Int32",
        "VER014__Personenkilometer__1000": "Int32",
    },
)

# %%
column_mapping = {
    "Zeit": "Jahr",
    "2_Auspraegung_Code": "Quartal",
    "3_Auspraegung_Label": "Verkehrsart",
    "UNT003__Unternehmen__Anzahl": "Unternehmen__Anzahl",
    "VER013__Befoerderte_Personen__1000": "Befoerderte_Personen__1000",
    "VER014__Personenkilometer__1000": "Personenkilometer__1000",
}

df = df[column_mapping.keys()]
df.columns = column_mapping.values()

# %%
filter_verkehrsart = df["Verkehrsart"] == "Liniennahverkehr insgesamt"
df = df[filter_verkehrsart]

# %%
qs = df["Quartal"].str.extract(r".*(\d)")
ys = df["Jahr"].apply(str)
df["Quartal"] = ys.str.cat(qs, sep="Q")


# %%
df.to_csv("oepnv-fahrgaeste.csv", index=False)

