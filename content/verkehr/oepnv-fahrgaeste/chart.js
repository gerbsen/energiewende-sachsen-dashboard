document.addEventListener("DOMContentLoaded", function () {
  var vlSpec = {
    width: 'container',
    height: 'container',
    data: {
      url: '/verkehr/oepnv-fahrgaeste/oepnv-fahrgaeste.csv'
    },
    mark: { type: 'bar', tooltip: true },
    encoding: {
      y: {
        aggregate: 'sum',
        field: 'Befoerderte_Personen__1000',
        type: 'quantitative',
        axis: { title: 'Fahrgäste (Tsd.)' }
      },
      x: { field: 'Jahr', type: 'ordinal' }
    }

  }

  vegaEmbed('#oepnv-fahrgaeste-chart', vlSpec);
})