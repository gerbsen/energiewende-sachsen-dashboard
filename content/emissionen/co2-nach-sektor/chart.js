document.addEventListener("DOMContentLoaded", function () {
  var vlSpec = {
    width: 'container',
    height: 'container',
    data: {
      url: '/emissionen/co2-nach-sektor/co2-nach-sektor.csv'
    },
    layer: [
      {
        mark: { type: 'bar' },
        encoding: {
          y: { field: 'Emissionen (kt CO2)', type: 'quantitative', title: 'Emissionen (kt CO2)' },
          x: { field: 'Jahr', type: 'ordinal' },
          tooltip: [
            { field: 'Jahr', type: 'ordinal' },
            { field: 'Sektor', type: 'ordinal' },
            { field: 'Emissionen (kt CO2)', type: 'quantitative', format: '.2f' },
          ],
          color: { field: 'Sektor', type: 'nominal', scale: { scheme: 'accent' }, legend: { orient: 'bottom', labelLimit: 300 } }
        }
      }, {
        mark: { type: 'text', dy: -10 },
        encoding: {
          y: { aggregate: 'sum', field: 'Emissionen (kt CO2)', type: 'quantitative' },
          x: { field: 'Jahr', type: 'ordinal' },
          text: { field: 'Emissionen (kt CO2)', aggregate: 'sum', type: 'ordinal', format: ',d' }
        }
      }
    ]

  }

  vegaEmbed('#co2-nach-sektor-chart', vlSpec);
})