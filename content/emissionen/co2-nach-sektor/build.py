# %%
import pandas as pd


# %%
df = pd.read_csv(
    "./lak-co2-emissionen-nach-emittentensektoren.csv",
    encoding="latin1",
    sep=";",
    skiprows=4,
    nrows=9,
)

df.columns = [
    "Land",
    "Jahr",
    "Anmerkung",
    "Insgesamt",
    "Gew. Steine u. Erden, Bergbau, verarb. Gewerbe insg.",
    "Verkehr insgesamt",
    "Schienenverkehr",
    "Straßenverkehr",
    "Luftverkehr",
    "Küsten- und Binnenschifffahrt",
    "Haushalte, GHD, übrige Verbraucher",
    "Stand",
]

df = df[
    [
        "Jahr",
        "Gew. Steine u. Erden, Bergbau, verarb. Gewerbe insg.",
        "Verkehr insgesamt",
        "Haushalte, GHD, übrige Verbraucher",
    ]
]

df = df.melt(id_vars="Jahr", var_name="Sektor", value_name="Emissionen (kt CO2)")

# %%
df.to_csv("./co2-nach-sektor.csv", index=False)
