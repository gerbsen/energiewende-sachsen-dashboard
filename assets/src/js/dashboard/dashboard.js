/* globals Chart:false, feather:false */

(function () {
  'use strict'

  feather.replace()

  tippy('#bruttostromverbrauch-help', {
    content: "Der Bruttostromverbrauch umfasst den Endenergieverbrauch an Strom, sowie die damit einhergehenden Umwandlungs- und Übertragungsverluste. Er ist als Bezugsgröße für den Anteil der Erneuerbaren Energien in der EG-Richtlinie 2009/28/EG zur Förderung der Nutzung von Energie aus erneuerbaren Quellen festgelegt und neben dem Verbrauch von Wärme und Kraftstoffen ein wichtiger Bestandteil des Verbrauches von Endenergie. <br>Quelle: <a href='https://www.umweltbundesamt.de/service/glossar/b?tag=Bruttostromverbrauch#alphabar'>Umweltbundesamt</a>",
    allowHTML: true,
    interactive: true
  });

  // configure global vega number formats for de-DE locale
  var formatLocale = {
    "decimal": ",",
    "thousands": ".",
    "grouping": [3],
    "currency": ["", "\u00a0€"]
  }
  vega.formatLocale(formatLocale);
}())
