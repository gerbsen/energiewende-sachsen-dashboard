# Energiewende Sachsen Dashboard
Das Dashboard ist aktuell unter [https://klimadashboard.danielgerber.eu/](https://klimadashboard.danielgerber.eu/) erreichbar.

## Datenquellen
- [Länderarbeitskreis Energiebilanzen](http://www.lak-energiebilanzen.de/)
- [Statistisches Landesamt Sachsen](https://www.statistik.sachsen.de/genonline/online/logon)
- [Sammlung weiterer potentiell interessanter Datenquellen](https://airtable.com/shr5O2Sfyh2FGFlEj)
- [Antwort des SMEKUL auf große Anfrage](http://edas.landtag.sachsen.de/viewer.aspx?dok_nr=1526&dok_art=Drs&leg_per=7&pos_dok=&dok_id=263407)
- [Wo steht die sächsische Energiewende 2020? - Hans-Jürgen Schlegel | Klimaschutzreferent aD](https://www.youtube.com/watch?v=IjjHicjH4sY)
- [Corona & die Energiewende in Sachsen | Im Gespräch mit Staatssekretär Dr. Gerd Lippold | VEE Sachsen](https://www.youtube.com/watch?v=k-ZJ48yreTs)

## Setup
Um das Dashboard lokal zu starten sollte [Docker installiert](https://docs.docker.com/desktop/) sein. 

```
docker run --rm -it \
  -v $(pwd):/src \
  -p 1313:1313 \
  klakegg/hugo:0.80.0 \
  server
```

Danach ist das Dashboard im Browser unter `http://localhost:1313` abrufbar.

Mehr Details zur Nutzung des Docker Images finden sich in [diesem Repository](https://github.com/klakegg/docker-hugo).

### Server
Wir überwachen die Container Registry auf Gitlab für neue Docker-Images des `develop`-Branchs. Also immer wenn ein Feature-Branch in den Develop-Branch gemerged wird, wird auch ein neues Image gebaut, dass wir mittels WatchTower überwachen und redeployen.

```
// start the dashboard watchtower
docker run --name buero_nginx_klimaboard -p $PORT:80 -d registry.gitlab.com/gerbsen/energiewende-sachsen-dashboard:develop

// start watchtower
docker run -d \
  --name watchtower \
  --restart always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  containrrr/watchtower buero_nginx_klimaboard --debug --interval=60
```

## Development

### Branches und Merge Requests
Entwickelt wird mit Hilfe des Gitflow-Prinzips. 
Mehr Informationen zu diesem Prinzip findet man [hier](https://www.youtube.com/watch?v=1SXpE08hvGs).

### Directory Layout

Die Verzeichnisstruktur folgt dem Standard von Hugo. [(docs)](https://gohugo.io/getting-started/directory-structure/)

`/layouts`

HTML Templates in Go's `html/template` syntax. [(docs)](https://gohugo.io/templates/introduction/)
* `baseof.html` - Basis Layout für alle Seiten
* `home.html` - Layout für die Startseite (`content/_index.html`)

`/content`

Der eigentliche Inhalt der Seiten. Die URLs ergeben sich aus der Verzeichnisstruktur. Unterverzeichnisse repräsentieren bei Hugo sogenannte [Page Bundles](https://gohugo.io/content-management/organization/), die ausser dem Inhalt noch weitere Resourcen (Bilder, Javascript, Dateien) enthalten können.

`assets`

Assets (CSS, Javascript) die in die Seiten eingebunden werden. [(docs)](https://gohugo.io/hugo-pipes/introduction/#asset-directory)

`/public`

Die gerenderten HTML Seiten. Sollte nicht eingecheckt werden, da die Dateien von Hugo generiert werden.

`/static`

Sonstiger statischer Inhalt der unverändert ausgeliefert wird.

### Navigation

Die Navigation in der Seiteleiste wird automatisch generiert. Die 1. Ebene entspricht der ersten Verzeichnisebene im `content` Verzeichnis ("Energie", "Verkehr"...). Seiten in deren Unterverzeichnissen erscheinen als Einträge auf der 2. Navigationsebene. Der Name des Unternavigationspunkts kommt aus dem Frontmatter (Schlüssel: "title") der jeweiligen Seite.

Beispiel:

`content/energie/erneuerbare-stromverbrauch/index.html`
```
---
title: Anteil Erneuerbare
---
(Seiteninhalt...)
```

Erscheint in der Navigation:
```
Energie
  - Anteil Erneuerbare
```

### Visualisierung

Die Grafiken werden mit [Vega Lite](https://vega.github.io/vega-lite/) erstellt: [docs](https://vega.github.io/vega-lite/docs/) | [tutorial](https://vega.github.io/vega-lite/tutorials/getting_started.html)
Mit Hilfe von [Altair](https://altair-viz.github.io/index.html) kann dies auch in Python Notebooks verwendet werden.

### Data Preparation

In dem meissten Fällen müssen die Rohdaten vor der Benutzung in der Visualisierung vorbereitet werden. In diesem Projekt kommt dafür Python und [Pandas](https://pandas.pydata.org/) zum Einsatz. Für die Liste der benutzten Bibliotheken siehe `tools/python/requirements.txt`. Die Entwicklungsumgebung initialisiert man z.B. wie folgt:

```
# non-python dependencies installieren
brew install spatialindex gdal

# virtualenv initialisieren und python dependencies installieren (einmalig)
make venv

# virtualenv nutzen
source venv/bin/activate
```

Alternativ kann auch Docker verwendet werden:
```
# docker image bauen
make python-image

# docker image interaktiv nutzen
docker run -it --rm -v $(pwd):/usr/src/app klimadashboard/python bash

# oder zum einmaligen script durchlauf:
docker run -it --rm \
  -v $(pwd)/pfad/zum/dataset:/usr/src/app \
  klimadashboard/python \
  python build.py
```

## Quellen/Lizenzen
- Logo:
  - Warming Stripes für Sachsen von [Ed Hawkins (University of Reading)](https://showyourstripes.info/) Attribution 4.0 International (CC BY 4.0) 
  - Sachsen Wappen: [Wikipedia](https://en.wikipedia.org/wiki/File:Coat_of_arms_of_Saxony.svg) Public Domain
