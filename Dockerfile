FROM klakegg/hugo:0.80.0-onbuild AS hugo

FROM nginx:1.19.6-alpine

COPY --from=hugo /target /usr/share/nginx/html
