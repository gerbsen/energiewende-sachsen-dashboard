# Python Entwicklungsumgebung

## Data Preparation
* [pandas](https://pandas.pydata.org/)
* [geopandas](https://geopandas.org/)
* [daff](https://github.com/paulfitz/daff)

## Prototyping / Notebooks
* [altair](https://altair-viz.github.io/)
* [folium](https://python-visualization.github.io/folium/)
* [VSCode Interactive Python](https://code.visualstudio.com/docs/python/jupyter-support-py)
